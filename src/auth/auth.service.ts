// import { BadRequestException, Injectable } from '@nestjs/common';
// import { InjectModel } from '@nestjs/sequelize';
// import { CreateUserDto } from 'src/users/dto/create-user.dto';
// import { User } from 'src/users/models/user.model';
// import {JwtService} from '@nestjs/jwt'
// import * as bcrypt from 'bcrypt'

// @Injectable()
// export class AuthService {
//   constructor(@InjectModel(User)
//     private readonly userRepo: typeof User,
//     private readonly jwtService: JwtService,
//     ){}

//     async registration(createUserDto: CreateUserDto, res: Response) {
//       const user = await this.userRepo.findOne({
//         where: { username: createUserDto.username },
//       });
//       if (user) {
//         throw new BadRequestException('Username already exists');
//       }

//       if (createUserDto.password !== createUserDto.confirm_password) {
//         throw new BadRequestException('Passwords is not match');
//       }
//       const hashed_password = await bcrypt.hash(createUserDto.password, 7);
//       const newUser = await this.userRepo.create({
//         ...createUserDto,
//         hashed_password: hashed_password,
//       });
//       const tokens = await this.getTokens(
//         newUser.id,
//         newUser.is_active,
//         newUser.is_owner,
//       );

//       const hashedRefreshToken = await bcrypt.hash(tokens.refresh_token, 7);
//       const updatedUser = await this.userRepo.update(
//         {
//           hashed_refresh_token: hashedRefreshToken,
//         },
//         { where: { id: newUser.id }, returning: true },
//       );
//       res.cookie('refresh_token', tokens.refresh_token, {
//         maxAge: 15 * 24 * 60 * 60 * 1000,
//         httpOnly: true,
//       });
//       const response = {
//         message: 'User registered successfully',
//         user: updatedUser[1][0],
//         tokens: tokens,
//       };

//       return response;
//     }

//   async getTokens(user_id: number, is_active: boolean, is_owner: boolean) {
//     const jwtPayload = {
//       id: user_id,
//       is_active,
//       is_owner,
//     };
//     const [accessToken, refreshToken] = await Promise.all([
//       this.jwtService.signAsync(jwtPayload, {
//         secret: process.env.ACCESS_TOKEN_KEY,
//         expiresIn: process.env.ACCESS_TOKEN_TIME,
//       }),
//       this.jwtService.signAsync(jwtPayload, {
//         secret: process.env.REFRESH_TOKEN_KEY,
//         expiresIn: process.env.REFRESH_TOKEN_TIME,
//       }),
//     ]);
//     return {
//       access_token: accessToken,
//       refresh_token: refreshToken,
//     };
//   }
// }
