// import { Controller, Get, Post, Body, Patch, Param, Delete, Res } from '@nestjs/common';
// import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
// import { CreateUserDto } from 'src/users/dto/create-user.dto';
// import { User } from 'src/users/models/user.model';
// import { AuthService } from './auth.service';
// import { CreateAuthDto } from './dto/create-auth.dto';
// import { UpdateAuthDto } from './dto/update-auth.dto';

// @ApiTags("Autorizatsiya")
// @Controller('auth')
// export class AuthController {
//   constructor(private readonly authService: AuthService) {}
//   @ApiOperation({ summary: 'Registration a new user' })
//   @ApiResponse({ status: 201, type: User })
//   @Post('signup')
//   registration(
//     @Body() registerUserDto: CreateUserDto,
//     @Res({ passthrough: true }) res: Response,
//   ) {
//     return this.authService.registration(registerUserDto, res);
//   }

// }
