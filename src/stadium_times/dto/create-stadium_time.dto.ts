import { ApiProperty } from '@nestjs/swagger';

export class CreateStadiumTimeDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  stadium_id: number;

  @ApiProperty({ example: '7:00', description: 'Boshlanish vaqti' })
  start_time: string;

  @ApiProperty({ example: '9:00', description: 'Tugash vaqti' })
  end_time: string;

  @ApiProperty({ example: 100000, description: 'Narxi' })
  price: number;
}
