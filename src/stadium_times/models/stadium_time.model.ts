import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { Stadium } from 'src/stadiums/models/stadium.model';

interface CreateStTimeAttr {
  stadium_id: number;
  start_time: string;
  end_time: string;
  price: number;
}

@Table({ tableName: 'stadium_times' })
export class StadiumTime extends Model<StadiumTime, CreateStTimeAttr> {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 2, description: 'Tashqi kalit' })
  @ForeignKey(() => Stadium)
  @Column({
    type: DataType.INTEGER,
  })
  stadium_id: number;
  @BelongsTo(() => Stadium)
  stadium: Stadium;

  @ApiProperty({ example: '7:00', description: 'Boshlash vaqti' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  start_time: string;

  @ApiProperty({ example: '9:00', description: 'Tugash vaqti' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  end_time: string;

  @ApiProperty({ example: 100000, description: 'Narxi' })
  @Column({
    type: DataType.BIGINT,
    allowNull: false,
  })
  price: number;
}
