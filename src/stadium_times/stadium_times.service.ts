import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateStadiumTimeDto } from './dto/create-stadium_time.dto';
import { UpdateStadiumTimeDto } from './dto/update-stadium_time.dto';
import { StadiumTime } from './models/stadium_time.model';

@Injectable()
export class StadiumTimesService {
  constructor(
    @InjectModel(StadiumTime)
    private readonly stadiumTimeRepo: typeof StadiumTime,
  ) {}
  async create(createStadiumTimeDto: CreateStadiumTimeDto) {
    const stadiumTime = await this.stadiumTimeRepo.create(createStadiumTimeDto);
    const response = {
      message: 'StadiumTime created successfully',
      time: stadiumTime,
    };
    return response;
  }

  findAll() {
    return this.stadiumTimeRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const stadiumTime = await this.stadiumTimeRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!stadiumTime) {
      throw new BadRequestException('Stadium time not found');
    }
    return stadiumTime;
  }

  async update(id: number, updateStadiumTimeDto: UpdateStadiumTimeDto) {
    const stadiumTime = this.stadiumTimeRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!stadiumTime) {
      throw new BadRequestException('Stadium time not found');
    }
    const updateStadiumTime = await this.stadiumTimeRepo.update(
      { ...updateStadiumTimeDto },
      { where: { id }, returning: true },
    );
    return updateStadiumTime;
  }

  async remove(id: number) {
    const stadiumTime = this.stadiumTimeRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!stadiumTime) {
      throw new BadRequestException('Stadium time not found');
    }
    await this.stadiumTimeRepo.destroy({ where: { id } });
    return `This action removes a #${id} stadiumTime`;
  }
}
