import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { User } from 'src/users/models/user.model';

@Table({ tableName: 'user_wallet' })
export class UserWallet extends Model<UserWallet> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 2, description: 'Tashqi kalit' })
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  user_id: number;
  @BelongsTo(() => User)
  user: User;

  @ApiProperty({ example: 'hamyon', description: 'Foydalanuvchi hamyoni' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  wallet: string;
}
