import { ApiProperty } from '@nestjs/swagger';

export class CreateUserWalletDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  user_id: number;

  @ApiProperty({ example: 'hamyon', description: 'Foydalanuvchi hamyoni' })
  wallet: string;
}
