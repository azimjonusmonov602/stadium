import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { CreateUserWalletDto } from './create-user_wallet.dto';

export class UpdateUserWalletDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  user_id?: number;

  @ApiProperty({ example: 'hamyon', description: 'Foydalanuvchi hamyoni' })
  wallet?: string;
}
