import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateUserWalletDto } from './dto/create-user_wallet.dto';
import { UpdateUserWalletDto } from './dto/update-user_wallet.dto';
import { UserWallet } from './models/user_wallet.models';

@Injectable()
export class UserWalletService {
  constructor(
    @InjectModel(UserWallet) private readonly userWalletRepo: typeof UserWallet,
  ) {}
  async create(createUserWalletDto: CreateUserWalletDto) {
    const userWallet = await this.userWalletRepo.create(createUserWalletDto);
    const response = {
      message: 'Created user wallet successfully',
      wallet: userWallet,
    };
    return response;
  }

  findAll() {
    return this.userWalletRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const userWallet = await this.userWalletRepo.findOne({ where: { id: id } });
    if (!userWallet) {
      throw new BadRequestException('Wallet not found');
    }
    return userWallet;
  }

  async update(id: number, updateUserWalletDto: UpdateUserWalletDto) {
    const userWallet = await this.userWalletRepo.findOne({ where: { id: id } });
    if (!userWallet) {
      throw new BadRequestException('Wallet not found');
    }
    const updatedWallet = await this.userWalletRepo.update(
      { ...updateUserWalletDto },
      { where: { id: id } },
    );
    const response = {
      message: 'Updated wallet successfully',
      wallet: updatedWallet,
    };
    return response;
  }

  async remove(id: number) {
    const userWallet = await this.userWalletRepo.findOne({ where: { id: id } });
    if (!userWallet) {
      throw new BadRequestException('Wallet not found');
    }
    await this.userWalletRepo.destroy({ where: { id: id } });
    return `This action removes a #${id} userWallet`;
  }
}
