import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateCartDto } from './dto/create-cart.dto';
import { UpdateCartDto } from './dto/update-cart.dto';
import { Cart } from './models/cart.model';

@Injectable()
export class CartService {
  constructor(@InjectModel(Cart) private readonly cartRepo: typeof Cart) {}
  async create(createCartDto: CreateCartDto) {
    const newCart = await this.cartRepo.create(createCartDto);
    const response = {
      message: 'Cart created successfully',
      cart: newCart,
    };
    return response;
  }

  findAll() {
    return this.cartRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const cart = await this.cartRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!cart) {
      throw new BadRequestException('Cart not found');
    }
    return cart;
  }

  async update(id: number, updateCartDto: UpdateCartDto) {
    const cart = await this.cartRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!cart) {
      throw new BadRequestException('Cart not found');
    }
    const updateCart = await this.cartRepo.update(
      { ...updateCartDto },
      { where: { id } },
    );
    const response = {
      message: 'Updated cart successfully',
      cart: updateCart,
    };
    return response;
  }

  async remove(id: number) {
    const cart = await this.cartRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!cart) {
      throw new BadRequestException('Cart not found');
    }
    await this.cartRepo.destroy({ where: { id } });
    return `This action removes a #${id} cart`;
  }
}
