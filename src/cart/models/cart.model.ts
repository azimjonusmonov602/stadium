import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { StadiumTime } from 'src/stadium_times/models/stadium_time.model';
import { User } from 'src/users/models/user.model';
import { UserWallet } from 'src/user_wallet/models/user_wallet.models';

interface CreateCartAttr {
  user_id: number;
  user_wallet_id: number;
  st_times_id: number;
  date: Date;
  createdAt: Date;
  time_for_clear: string;
}
@Table({ tableName: 'cart' })
export class Cart extends Model<Cart, CreateCartAttr> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  user_id: number;
  @BelongsTo(() => User)
  user: User;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => UserWallet)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  user_wallet_id: number;
  @BelongsTo(() => UserWallet)
  user_wallet: UserWallet;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => StadiumTime)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  stadium_time_id: number;
  @BelongsTo(() => StadiumTime)
  stadium_time: StadiumTime;

  @ApiProperty({ example: '2000-01-01', description: 'Bugungi vaqt' })
  @Column({
    type: DataType.DATE,
    defaultValue: Date.now(),
  })
  date: Date;

  @ApiProperty({ example: '2000-01-01', description: 'Yaratilgan vaqti' })
  @Column({
    type: DataType.DATE,
    defaultValue: Date.now(),
  })
  createdAt: Date;

  @ApiProperty({
    example: '30m',
    description: 'Amal qilsih vaqti (Minut yoki soatda)',
  })
  @Column({
    type: DataType.STRING,
    defaultValue: '30m',
  })
  time_for_clear: string;

  // @ApiProperty({example: 1, description: "Tashqi kalit"})
  // @ForeignKey(() => Status)
  // @Column({
  //     type: DataType.INTEGER,
  //     allowNull: false
  // })
  // status_id: number
  // @BelongsTo(() => Status)
  // status: Status
}
