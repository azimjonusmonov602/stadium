import { ApiProperty } from '@nestjs/swagger';

export class CreateCartDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  user_id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  user_wallet_id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  stadium_time_id: number;

  @ApiProperty({ example: '2000-01-01', description: 'Bugungi vaqt' })
  date: Date;

  @ApiProperty({ example: '2000-01-01', description: 'Yaratilgan vaqti' })
  createdAt: Date;

  @ApiProperty({
    example: '30m',
    description: 'Amal qilsih vaqti (Minut yoki soatda)',
  })
  time_for_clear: string;

  // @ApiProperty({example: 1, description: "Tashqi kalit"})

  // status_id: number
}
