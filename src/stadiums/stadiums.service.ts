import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateStadiumDto } from './dto/create-stadium.dto';
import { UpdateStadiumDto } from './dto/update-stadium.dto';
import { Stadium } from './models/stadium.model';

@Injectable()
export class StadiumsService {
  constructor(
    @InjectModel(Stadium) private readonly stadiumRepo: typeof Stadium,
  ) {}
  async create(createStadiumDto: CreateStadiumDto) {
    return 'This action adds a new stadium';
  }

  findAll() {
    return `This action returns all stadiums`;
  }

  findOne(id: number) {
    return `This action returns a #${id} stadium`;
  }

  update(id: number, updateStadiumDto: UpdateStadiumDto) {
    return `This action updates a #${id} stadium`;
  }

  remove(id: number) {
    return `This action removes a #${id} stadium`;
  }
}
