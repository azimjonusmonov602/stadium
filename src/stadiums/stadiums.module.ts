import { Module } from '@nestjs/common';
import { StadiumsService } from './stadiums.service';
import { StadiumsController } from './stadiums.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Stadium } from './models/stadium.model';
import { Category } from 'src/category/models/category.model';
import { Region } from 'src/region/models/region.model';
import { District } from 'src/district/models/district.model';
import { User } from 'src/users/models/user.model';
import { JwtModule } from '@nestjs/jwt';
import { Comfort } from 'src/comfort/models/comfort.model';
import { ComfortStadium } from 'src/comfort/models/comfort-stadium.model';

@Module({
  imports: [
    SequelizeModule.forFeature([
      Stadium,
      Category,
      Region,
      District,
      User,
      Comfort,
      ComfortStadium,
    ]),
    JwtModule,
  ],
  controllers: [StadiumsController],
  providers: [StadiumsService],
})
export class StadiumsModule {}
