import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateStadiumDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @IsNotEmpty()
  @IsNumber()
  category_id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @IsNotEmpty()
  @IsNumber()
  owner_id: number;

  @ApiProperty({
    example: "Bog'lanish",
    description: "Bog'lanish uchun manbaa",
  })
  @IsNotEmpty()
  @IsString()
  contact_with: string;

  @ApiProperty({ example: 'Bunyodkor', description: 'Stadion nomi' })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({ example: 'Daraja', description: 'Daraja' })
  @IsNotEmpty()
  @IsString()
  volume: string;

  @ApiProperty({
    example: "Cho'pon ota ko'chasi 10-uy",
    description: 'Stadion manzili',
  })
  @IsNotEmpty()
  @IsString()
  address: string;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @IsNumber()
  region_id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @IsNumber()
  district_id: number;

  @ApiProperty({ example: 'location', description: 'Stadion joylashuvi' })
  @IsNotEmpty()
  @IsString()
  location: string;

  @ApiProperty({ example: '2000-01-01', description: 'Stadion qurilgan yili' })
  @IsNotEmpty()
  @IsDateString()
  buildAt: Date;

  @ApiProperty({ example: '7:00', description: 'Stadion ish boshlash vaqti' })
  @IsNotEmpty()
  @IsString()
  start_time: string;

  @ApiProperty({ example: '23:00', description: 'Stadion ish tugatish vaqti' })
  @IsNotEmpty()
  @IsString()
  end_time: string;
}
