import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateStadiumDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  category_id?: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  owner_id?: number;

  @ApiProperty({
    example: "Bog'lanish",
    description: "Bog'lanish uchun manbaa",
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  contact_with?: string;

  @ApiProperty({ example: 'Bunyodkor', description: 'Stadion nomi' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  name?: string;

  @ApiProperty({ example: 'Daraja', description: 'Daraja' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  volume?: string;

  @ApiProperty({
    example: "Cho'pon ota ko'chasi 10-uy",
    description: 'Stadion manzili',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  address?: string;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @IsOptional()
  @IsNumber()
  region_id?: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @IsOptional()
  @IsNumber()
  district_id?: number;

  @ApiProperty({ example: 'location', description: 'Stadion joylashuvi' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  location?: string;

  @ApiProperty({ example: '2000-01-01', description: 'Stadion qurilgan yili' })
  @IsOptional()
  @IsNotEmpty()
  @IsDateString()
  buildAt?: Date;

  @ApiProperty({ example: '7:00', description: 'Stadion ish boshlash vaqti' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  start_time?: string;

  @ApiProperty({ example: '23:00', description: 'Stadion ish tugatish vaqti' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  end_time?: string;
}
