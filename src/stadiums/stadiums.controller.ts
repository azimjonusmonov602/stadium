import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { StadiumsService } from './stadiums.service';
import { CreateStadiumDto } from './dto/create-stadium.dto';
import { UpdateStadiumDto } from './dto/update-stadium.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Stadium } from './models/stadium.model';
import { OwnerGuard } from 'src/guards/owner.guard';

@ApiTags('Stadiums-Stadionlar')
@Controller('stadiums')
export class StadiumsController {
  constructor(private readonly stadiumsService: StadiumsService) {}

  @ApiOperation({ summary: 'Yangi stadion yaratish' })
  @ApiResponse({ status: 201, type: Stadium })
  @UseGuards(OwnerGuard)
  @Post()
  create(@Body() createStadiumDto: CreateStadiumDto) {
    return this.stadiumsService.create(createStadiumDto);
  }

  @ApiOperation({ summary: 'Find all Stadiums' })
  @ApiResponse({ status: 200, type: Stadium })
  @Get()
  findAll() {
    return this.stadiumsService.findAll();
  }

  @ApiOperation({ summary: 'Find one stadium by id' })
  @ApiResponse({ status: 200, type: Stadium })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.stadiumsService.findOne(+id);
  }

  @ApiOperation({ summary: 'Update stadium by ID' })
  @ApiResponse({ status: 200, type: Stadium })
  @UseGuards(OwnerGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateStadiumDto: UpdateStadiumDto) {
    return this.stadiumsService.update(+id, updateStadiumDto);
  }

  @ApiOperation({ summary: 'Delete stadium by ID' })
  @ApiResponse({ status: 200, type: Stadium })
  @UseGuards(OwnerGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stadiumsService.remove(+id);
  }
}
