import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  BelongsToMany,
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  Table,
} from 'sequelize-typescript';
import { Category } from 'src/category/models/category.model';
import { ComfortStadium } from 'src/comfort/models/comfort-stadium.model';
import { Comfort } from 'src/comfort/models/comfort.model';
import { District } from 'src/district/models/district.model';
import { Region } from 'src/region/models/region.model';
import { User } from 'src/users/models/user.model';

interface CreateStadiumAttr {
  category_id: number;
  owner_id: number;
  contact_with: string;
  name: string;
  volume: string;
  address: string;
  region_id: number;
  district_id: number;
  location: string;
  buildAt: Date;
  start_time: string;
  end_time: string;
}
@Table({ tableName: 'stadium' })
export class Stadium extends Model<Stadium, CreateStadiumAttr> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 2, description: 'Tashqi kalit' })
  @ForeignKey(() => Category)
  @Column({
    type: DataType.INTEGER,
  })
  category_id: number;
  @BelongsTo(() => Category)
  category: Category;

  //   @HasMany(() => Category)
  //   categories: Category[];

  //   @HasMany(() => Stadium)
  //   stadiums: Stadium[];

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
  })
  owner_id: number;

  @BelongsTo(() => User)
  owner: User;

  @ApiProperty({
    example: "bog'lanish uchun manba",
    description: "Bog'lanish uchun manba",
  })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  contact_wiht: string;

  @ApiProperty({ example: 'Bunyodkor', description: 'Stadion nomi' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @ApiProperty({ example: 'Darajasi', description: 'Stadionning darajasi' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  volume: string;

  @ApiProperty({
    example: "Cho'ponota ko'chasi 10-uy",
    description: 'Stadion manzili',
  })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  address: string;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => Region)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  region_id: number;
  @BelongsTo(() => Region)
  region: Region;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => District)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  district_id: number;
  @BelongsTo(() => District)
  district: District;

  @ApiProperty({ example: 'location', description: 'Stadion joylashuvi' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  location: string;

  @ApiProperty({ example: '2000-01-01', description: 'Stadion qurilgan yili' })
  @Column({
    type: DataType.DATE,
    allowNull: false,
  })
  buildAt: Date;

  @ApiProperty({ example: '7:00', description: 'Stadion ish boshlash vaqti' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  start_time: string;

  @ApiProperty({ example: '7:00', description: 'Stadion ish boshlash vaqti' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  end_time: string;

  @BelongsToMany(() => Comfort, () => ComfortStadium)
  comforts: Comfort[];
}
