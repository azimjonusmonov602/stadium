import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Category } from './models/category.model';

@Injectable()
export class CategoryService {
  constructor(
    @InjectModel(Category) private readonly categoryRepo: typeof Category,
  ) {}
  async create(createCategoryDto: CreateCategoryDto) {
    const category = await this.categoryRepo.findOne({
      where: { name: createCategoryDto.name },
    });
    if (category) {
      throw new BadRequestException('This category already exists');
    }
    const newCategory = await this.categoryRepo.create(createCategoryDto);
    const response = {
      message: 'Create category successfully',
      category: newCategory,
    };
    return response;
  }

  findAll() {
    return this.categoryRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const category = await this.categoryRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!category) {
      throw new BadRequestException('Category not found');
    }
    return category;
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    const category = await this.categoryRepo.findOne({ where: { id } });
    if (!category) {
      throw new BadRequestException('Category not found');
    }
    const updatedCategory = await this.categoryRepo.update(
      { ...updateCategoryDto },
      { where: { id }, returning: true },
    );
    const response = {
      message: 'Update category successfully',
      category: updatedCategory,
    };
    return response;
  }

  async remove(id: number) {
    const category = await this.categoryRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!category) {
      throw new BadRequestException('Category not found');
    }
    await this.categoryRepo.destroy({ where: { id } });
    const response = {
      message: 'Deleted category',
      deletedCategory: category,
    };
    return response;
  }
}
