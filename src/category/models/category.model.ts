import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  Table,
} from 'sequelize-typescript';
import { Stadium } from 'src/stadiums/models/stadium.model';

interface CreateCategoryAttr {
  name: string;
}
@Table({ tableName: 'category' })
export class Category extends Model<Category, CreateCategoryAttr> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas ID' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 'Futbol', description: 'Kategoriya nomi' })
  @Column({
    type: DataType.STRING,
    unique: true,
  })
  name: string;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => Category)
  @Column({
    type: DataType.SMALLINT,
  })
  parent_id: number;
  @BelongsTo(() => Category)
  category: Category;

  // @HasMany(() => Category)
  // categories: Category[];

  // @HasMany(() => Stadium)
  // stadiums: Stadium[];
}
