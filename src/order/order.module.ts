import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Order } from './models/order.model';
import { User } from 'src/users/models/user.model';
import { UserWallet } from 'src/user_wallet/models/user_wallet.models';
import { StadiumTime } from 'src/stadium_times/models/stadium_time.model';
import { Status } from 'src/status/models/status.model';

@Module({
  imports: [
    SequelizeModule.forFeature([Order, User, UserWallet, StadiumTime, Status]),
  ],
  controllers: [OrderController],
  providers: [OrderService],
})
export class OrderModule {}
