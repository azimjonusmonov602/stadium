import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './models/order.model';

@Injectable()
export class OrderService {
  constructor(@InjectModel(Order) private readonly orderRepo: typeof Order) {}
  async create(createOrderDto: CreateOrderDto) {
    const newOrder = await this.orderRepo.create(createOrderDto);
    const response = {
      message: 'Create order successfully',
      order: newOrder,
    };
    return response;
  }

  findAll() {
    return this.orderRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const order = await this.orderRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!order) {
      throw new BadRequestException('Order not found');
    }
    return order;
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.orderRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!order) {
      throw new BadRequestException('Order not found');
    }
    const updatedOrder = await this.orderRepo.update(
      { ...updateOrderDto },
      { where: { id }, returning: true },
    );
    const response = {
      message: 'Updated order successfully',
      order: updatedOrder,
    };
    return response;
  }

  async remove(id: number) {
    const order = await this.orderRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!order) {
      throw new BadRequestException('Order not found');
    }
    await this.orderRepo.destroy({ where: { id } });
    return `This action removes a #${id} order`;
  }
}
