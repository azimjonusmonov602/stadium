import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User } from './users/models/user.model';
import { UsersModule } from './users/users.module';
import { AdminModule } from './admin/admin.module';
import { Admin } from './admin/models/admin.model';
import { JwtModule } from '@nestjs/jwt';
import { MailModule } from './mail/mail.module';
import { CategoryModule } from './category/category.module';
import { Category } from './category/models/category.model';
import { ComfortModule } from './comfort/comfort.module';
import { StadiumsModule } from './stadiums/stadiums.module';
import { RegionModule } from './region/region.module';
import { DistrictModule } from './district/district.module';
import { Stadium } from './stadiums/models/stadium.model';
import { Region } from './region/models/region.model';
import { District } from './district/models/district.model';
import { Comfort } from './comfort/models/comfort.model';
import { ComfortStadium } from './comfort/models/comfort-stadium.model';
import { StadiumTimesModule } from './stadium_times/stadium_times.module';
import { StadiumTime } from './stadium_times/models/stadium_time.model';
import { UserWalletModule } from './user_wallet/user_wallet.module';
import { UserWallet } from './user_wallet/models/user_wallet.models';
import { UserCardsModule } from './user_cards/user_cards.module';
import { UserCard } from './user_cards/models/user_card.model';
import { CartModule } from './cart/cart.module';
import { TelegrafModule } from 'nestjs-telegraf';
import { BotModule } from './bot/bot.module';
import { BOT_NAME } from './app.constants';
import { Bot } from './bot/models/bot.model';
import { OtpModule } from './otp/otp.module';
import { OrderModule } from './order/order.module';
import { StatusModule } from './status/status.module';
import { Status } from './status/models/status.model';
import { Order } from './order/models/order.model';
import { MediaModule } from './media/media.module';
import { Media } from './media/models/media.model';
import { CommentModule } from './comment/comment.module';
import { Comment } from './comment/models/comment.model';
import { GraphQLModule } from '@nestjs/graphql';

@Module({
  imports: [
    TelegrafModule.forRootAsync({
      botName: BOT_NAME,
      useFactory: () => ({
        token: process.env.BOT_TOKEN,
        middlewares: [],
        include: [BotModule],
      }),
    }),
    ConfigModule.forRoot({
      envFilePath: `.${process.env.NODE_ENV}.env`,
      isGlobal: true,
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: process.env.POSTGERS_HOST,
      port: Number(process.env.POSTGERS_PORT),
      username: process.env.POSTGRES_USER,
      password: String(process.env.POSTGRES_PASSWORD),
      database: process.env.POSTGRES_DB,
      models: [
        User,
        Admin,
        Category,
        Stadium,
        Region,
        District,
        Comfort,
        ComfortStadium,
        StadiumTime,
        UserWallet,
        UserCard,
        Bot,
        Status,
        Order,
        Media,
        Comment,
      ],
      autoLoadModels: true,
      logging: false,
    }),
    UsersModule,
    AdminModule,
    JwtModule,
    MailModule,
    CategoryModule,
    ComfortModule,
    StadiumsModule,
    RegionModule,
    DistrictModule,
    StadiumTimesModule,
    UserWalletModule,
    UserCardsModule,
    CartModule,
    BotModule,
    OtpModule,
    OrderModule,
    StatusModule,
    MediaModule,
    CommentModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
