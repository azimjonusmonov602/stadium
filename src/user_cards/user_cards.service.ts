import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from 'src/users/models/user.model';
import { CreateUserCardDto } from './dto/create-user_card.dto';
import { UpdateUserCardDto } from './dto/update-user_card.dto';
import { UserCard } from './models/user_card.model';

@Injectable()
export class UserCardsService {
  constructor(
    @InjectModel(UserCard) private readonly userCradRepo: typeof UserCard,
    @InjectModel(User) private readonly userRepo: typeof User,
  ) {}
  async create(createUserCardDto: CreateUserCardDto) {
    const userCard = await this.userCradRepo.findOne({
      where: { name: createUserCardDto.name },
    });
    if (userCard) {
      throw new BadRequestException('This card already exists');
    }
    const user = await this.userRepo.findOne({
      where: { id: createUserCardDto.user_id },
    });
    if (user.phone !== createUserCardDto.phone) {
      throw new BadRequestException('Phone number is not  match');
    }
    const newCard = await this.userCradRepo.create(createUserCardDto);
    const response = {
      message: 'Added card successfully',
      card: newCard,
    };
    return response;
  }

  findAll() {
    return this.userCradRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const card = await this.userCradRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!card) {
      throw new BadRequestException('Card not found');
    }
    return card;
  }

  async update(id: number, updateUserCardDto: UpdateUserCardDto) {
    const card = await this.userCradRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!card) {
      throw new BadRequestException('Card not found');
    }
    const updateUserCard = await this.userCradRepo.update(
      { ...updateUserCardDto },
      { where: { id: id } },
    );
    return updateUserCard;
  }

  async remove(id: number) {
    const card = await this.userCradRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!card) {
      throw new BadRequestException('Card not found');
    }
    await this.userCradRepo.destroy({ where: { id } });
    return `This action removes a #${id} userCard`;
  }
}
