import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { User } from 'src/users/models/user.model';

interface CreateUserCardAttr {
  user_id: number;
  name: string;
  phone: string;
  number: string;
  year: number;
  month: number;
}
@Table({ tableName: 'user_card' })
export class UserCard extends Model<UserCard, CreateUserCardAttr> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  user_id: number;

  @BelongsTo(() => User)
  user: User;

  @ApiProperty({ example: 'Humo', description: 'Foydalanuvchi karta nomi' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @ApiProperty({
    example: '+998901234567',
    description: "Foydalanuvchi kartasi bog'langan telefon raqam",
  })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  phone: string;

  @ApiProperty({
    example: '1111 2222 3333 4444',
    description: 'Foydalanuvchi kartasi raqami',
  })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  number: string;

  @ApiProperty({
    example: '2023',
    description: 'Foydalanuvchi kartasining amal qilish muddati yili',
  })
  @Column({
    type: DataType.BIGINT,
    allowNull: false,
  })
  year: number;

  @ApiProperty({
    example: '12',
    description: 'Foydalanuvchi kartasining amal qilish muddati oyi',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  month: number;

  @ApiProperty({
    example: false,
    description: 'Foydalanuvchi kartasi faol yoki faolmasligi',
  })
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  is_active: boolean;

  @ApiProperty({
    example: false,
    description: 'Foydalanuvchi kartasi asosiy yoki asosiy emasligi',
  })
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  is_main: boolean;
}
