import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsPhoneNumber, IsString } from 'class-validator';

export class CreateUserCardDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  user_id: number;

  @ApiProperty({ example: 'Humo', description: 'Foydalanuvchi karta nomi' })
  @IsString()
  name: string;

  @ApiProperty({
    example: '+998901234567',
    description: "Foydalanuvchi kartasi bog'langan telefon raqam",
  })
  @IsPhoneNumber()
  phone: string;

  @ApiProperty({
    example: '1111 2222 3333 4444',
    description: 'Foydalanuvchi kartasi raqami',
  })
  @IsString()
  number: string;

  @ApiProperty({
    example: '2023',
    description: 'Foydalanuvchi kartasining amal qilish muddati yili',
  })
  @IsNumber()
  year: number;

  @ApiProperty({
    example: '12',
    description: 'Foydalanuvchi kartasining amal qilish muddati oyi',
  })
  @IsNumber()
  month: number;

  @ApiProperty({
    example: false,
    description: 'Foydalanuvchi kartasi faol yoki faolmasligi',
  })
  @IsBoolean()
  is_active: boolean;

  @ApiProperty({
    example: false,
    description: 'Foydalanuvchi kartasi asosiy yoki asosiy emasligi',
  })
  @IsBoolean()
  is_main: boolean;
}
