import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateDistrictDto } from './dto/create-district.dto';
import { UpdateDistrictDto } from './dto/update-district.dto';
import { District } from './models/district.model';

@Injectable()
export class DistrictService {
  constructor(
    @InjectModel(District) private readonly districtRepo: typeof District,
  ) {}
  async create(createDistrictDto: CreateDistrictDto) {
    const district = await this.districtRepo.findOne({
      where: {
        name: createDistrictDto.name,
        region_id: createDistrictDto.region_id,
      },
    });
    if (district) {
      throw new BadRequestException('This district already exists');
    }
    const newDistrict = await this.districtRepo.create(createDistrictDto);
    const response = {
      message: 'Created new district',
      district: newDistrict,
    };
    return response;
  }

  findAll() {
    return this.districtRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const district = await this.districtRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!district) {
      throw new BadRequestException('District not found not');
    }

    return district;
  }

  async update(id: number, updateDistrictDto: UpdateDistrictDto) {
    const district = await this.districtRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!district) {
      throw new BadRequestException('District not found not');
    }
    const updatedDistrict = await this.districtRepo.update(
      { ...updateDistrictDto },
      { where: { id }, returning: true },
    );
    const response = {
      message: 'Updated district successfully',
      district: updatedDistrict,
    };
    return response;
  }

  async remove(id: number) {
    const district = await this.districtRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!district) {
      throw new BadRequestException('District not found not');
    }
    await this.districtRepo.destroy({ where: { id } });
    const response = {
      message: 'District deleted successfully',
      district: district,
    };
    return response;
  }
}
