import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { CreateDistrictDto } from './create-district.dto';

export class UpdateDistrictDto {
  @ApiProperty({ example: 'Chilonzor', description: 'Tuman nomi' })
  @IsOptional()
  @IsString()
  name?: string;
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  region_id?: number;
}
