import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateDistrictDto {
  @ApiProperty({ example: 'Chilonzor', description: 'Tuman nomi' })
  @IsString()
  name: string;
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  region_id: number;
}
