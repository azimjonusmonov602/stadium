import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { Region } from 'src/region/models/region.model';

interface CreateDistricAttr {
  name: string;
  region_id: number;
}
@Table({ tableName: 'district' })
export class District extends Model<District, CreateDistricAttr> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 'Chilonzor', description: 'Tuman nomi' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => Region)
  @Column({
    type: DataType.INTEGER,
  })
  region_id: number;

  @BelongsTo(() => Region)
  region: Region;
}
