import {
  BadGatewayException,
  BadRequestException,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateComfortDto } from './dto/create-comfort.dto';
import { UpdateComfortDto } from './dto/update-comfort.dto';
import { Comfort } from './models/comfort.model';

@Injectable()
export class ComfortService {
  constructor(
    @InjectModel(Comfort) private readonly comfortRepo: typeof Comfort,
  ) {}
  async create(createComfortDto: CreateComfortDto) {
    const comfort = await this.comfortRepo.findOne({
      where: { name: createComfortDto.name },
    });
    if (comfort) {
      throw new BadGatewayException('This compfort already exists');
    }
    const newComfort = await this.comfortRepo.create(createComfortDto);
    const response = {
      message: 'Created compfort',
      comfort: newComfort,
    };
    return response;
  }

  findAll() {
    return this.comfortRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const comfort = await this.comfortRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!comfort) {
      throw new BadRequestException('Comfort not found');
    }
    return comfort;
  }

  async update(id: number, updateComfortDto: UpdateComfortDto) {
    const comfort = await this.comfortRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!comfort) {
      throw new BadRequestException('Comfort not found');
    }
    const updatedComfort = await this.comfortRepo.update(
      { ...updateComfortDto },
      { where: { id } },
    );
    const response = {
      message: 'Comfort updated',
      comfort: updatedComfort,
    };
    return response;
  }

  async remove(id: number) {
    const comfort = await this.comfortRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!comfort) {
      throw new BadRequestException('Comfort not found');
    }
    await this.comfortRepo.destroy({ where: { id } });
    return `This action removes a ${id} comfort`;
  }
}
