import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { CreateComfortDto } from './create-comfort.dto';

export class UpdateComfortDto {
  @ApiProperty({ example: 'Dush', description: 'Stadion qulayliklari' })
  @IsString()
  name: string;
}
