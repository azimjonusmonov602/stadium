import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateComfortDto {
  @ApiProperty({ example: 'Dush', description: 'Stadion qulayliklari' })
  @IsString()
  name: string;
}
