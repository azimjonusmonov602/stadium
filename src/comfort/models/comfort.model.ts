import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsToMany,
  Column,
  DataType,
  Model,
  Table,
} from 'sequelize-typescript';
import { Stadium } from 'src/stadiums/models/stadium.model';
import { ComfortStadium } from './comfort-stadium.model';

interface CreateComforAttr {
  name: string;
}
@Table({ tableName: 'comfort' })
export class Comfort extends Model<Comfort, CreateComforAttr> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 'Dush', description: 'Stadion qulayliklari' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;
  @BelongsToMany(() => Stadium, () => ComfortStadium)
  stadiums: Comfort[];
}
