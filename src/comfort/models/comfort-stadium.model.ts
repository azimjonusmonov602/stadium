import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { Stadium } from 'src/stadiums/models/stadium.model';
import { Comfort } from './comfort.model';

@Table({ tableName: 'comfort_stadium' })
export class ComfortStadium extends Model<ComfortStadium> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => Stadium)
  @Column({
    type: DataType.INTEGER,
  })
  stadium_id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => Comfort)
  @Column({
    type: DataType.INTEGER,
  })
  comfort_id: number;
}
