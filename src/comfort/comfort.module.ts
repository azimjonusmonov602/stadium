import { Module } from '@nestjs/common';
import { ComfortService } from './comfort.service';
import { ComfortController } from './comfort.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Comfort } from './models/comfort.model';
import { JwtModule } from '@nestjs/jwt';
import { ComfortStadium } from './models/comfort-stadium.model';
import { Stadium } from 'src/stadiums/models/stadium.model';

@Module({
  imports: [
    SequelizeModule.forFeature([Comfort, ComfortStadium, Stadium]),
    JwtModule,
  ],
  controllers: [ComfortController],
  providers: [ComfortService],
})
export class ComfortModule {}
