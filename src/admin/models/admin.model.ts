import { ApiProperty } from '@nestjs/swagger';
import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface CreateAdminAttrs {
  username: string;
  email: string;
  telegram_link: string;
  phone: string;
  hashed_password: string;
  is_active: boolean;
  is_creator: boolean;
  hashed_refresh_token: string;
}
@Table({ tableName: 'admin' })
export class Admin extends Model<Admin, CreateAdminAttrs> {
  @ApiProperty({ example: 1, description: 'Unique ID' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  })
  id: number;

  @ApiProperty({ example: 'username', description: 'Admin username' })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  username: string;

  @ApiProperty({
    example: 'user@email.com',
    description: 'Admin email address',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  email: string;

  @ApiProperty({
    example: 'https://t.me/user',
    description: 'Foydalanuvchi pochta manzili',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  telegram_link: string;

  @ApiProperty({ example: '+998901234567', description: 'Admin phone' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  phone: string;

  @ApiProperty({ example: 'password', description: 'Hashed password' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  hashed_password: string;

  @ApiProperty({ example: false, description: 'Admin is active' })
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  is_active: boolean;

  @ApiProperty({ example: false, description: 'Admin is creator' })
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  is_creator: boolean;

  @ApiProperty({ example: 'token', description: 'Hashed refresh token' })
  @Column({
    type: DataType.STRING,
  })
  hashed_refresh_token: string;
}
