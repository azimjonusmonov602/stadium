import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsEmail,
  IsPhoneNumber,
  IsStrongPassword,
  MinLength,
} from 'class-validator';

export class CreateAdminDto {
  @ApiProperty({ example: 'username', description: 'Admin username' })
  @IsString()
  username: string;

  @ApiProperty({
    example: 'user@gmail.com',
    description: 'Admin email address',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    example: 'https://t.me/user',
    description: 'Foydalanuvchi pochta manzili',
  })
  telegram_link: string;

  @ApiProperty({ example: '+998901234567', description: 'Admin phone number' })
  @IsPhoneNumber()
  phone: string;

  @ApiProperty({ example: 'password', description: 'Admin password' })
  @IsStrongPassword()
  @MinLength(8)
  password: string;

  @ApiProperty({ example: 'password', description: 'Admin confirm password' })
  confirm_password: string;
}
