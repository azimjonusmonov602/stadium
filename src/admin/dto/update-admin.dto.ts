import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsPhoneNumber, IsString } from 'class-validator';
import { CreateAdminDto } from './create-admin.dto';

export class UpdateAdminDto {
  @ApiProperty({ example: 'username', description: 'Admin username' })
  @IsOptional()
  @IsString()
  username?: string;

  @ApiProperty({
    example: 'user@gmail.com',
    description: 'Admin email address',
  })
  @IsOptional()
  @IsEmail()
  email?: string;

  @ApiProperty({ example: '+998901234567', description: 'Admin phone number' })
  @IsOptional()
  @IsPhoneNumber()
  phone?: string;
}
