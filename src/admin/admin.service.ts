import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/sequelize';
import { CreateAdminDto } from './dto/create-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { Admin } from './models/admin.model';
import { Response } from 'express';
import * as bcrypt from 'bcrypt';
import { LoginAdminDto } from './dto/login-admin.dto';
import { UpdateAdminPassDto } from './dto/update-admin-password.dto';

@Injectable()
export class AdminService {
  constructor(
    @InjectModel(Admin)
    private readonly adminRepo: typeof Admin,
    private readonly jwtService: JwtService,
  ) {}

  async register(createAdminDto: CreateAdminDto, res: Response) {
    const admin = await this.adminRepo.findOne({
      where: { username: createAdminDto.username },
    });
    if (admin) {
      throw new BadRequestException('Username already exists');
    }
    if (createAdminDto.password !== createAdminDto.confirm_password) {
      throw new BadRequestException('Passwords is not match');
    }
    const hashed_password = await bcrypt.hash(createAdminDto.password, 7);
    const newAdmin = await this.adminRepo.create({
      ...createAdminDto,
      hashed_password: hashed_password,
    });
    const tokens = await this.getTokens(
      newAdmin.id,
      newAdmin.is_active,
      newAdmin.is_creator,
    );
    const hashed_refresh_token = await bcrypt.hash(tokens.refresh_token, 7);
    const updatedAdmin = await this.adminRepo.update(
      { hashed_refresh_token: hashed_refresh_token },
      { where: { id: newAdmin.id }, returning: true },
    );
    res.cookie('refresh_token', tokens.refresh_token, {
      maxAge: 15 * 24 * 60 * 60 * 1000,
      httpOnly: true,
    });
    console.log(updatedAdmin);
    const response = {
      message: 'User register successfully',
      admin: updatedAdmin,
      tokens: tokens,
    };
    return response;
  }
  async login(loginAdminDto: LoginAdminDto, res: Response) {
    const { email, password } = loginAdminDto;
    const admin = await this.adminRepo.findOne({ where: { email } });
    if (!admin) {
      throw new UnauthorizedException('Admin is not registered');
    }
    const passwordMatch = await bcrypt.compare(password, admin.hashed_password);
    if (!passwordMatch) {
      throw new UnauthorizedException('Admin is not registered(pass)');
    }
    const tokens = await this.getTokens(
      admin.id,
      admin.is_active,
      admin.is_creator,
    );
    const hashed_refresh_token = await bcrypt.hash(tokens.refresh_token, 7);
    res.cookie('refresh_token', tokens.refresh_token, {
      maxAge: 15 * 24 * 60 * 60 * 1000,
      httpOnly: true,
    });
    const updatedAdmin = await this.adminRepo.update(
      { hashed_refresh_token: hashed_refresh_token },
      { where: { id: admin.id }, returning: true },
    );
    const response = {
      message: 'Login admin succesfully',
      admin: updatedAdmin[1][0],
      tokens: tokens,
    };
    return response;
  }

  async logout(refreshToken: string, res: Response) {
    const adminData = await this.jwtService.verify(refreshToken, {
      secret: process.env.REFRESH_TOKEN_KEY,
    });
    if (!adminData) {
      throw new ForbiddenException('User not found');
    }

    const updatedAdmin = await this.adminRepo.update(
      { hashed_refresh_token: null },
      { where: { id: adminData.id }, returning: true },
    );
    res.clearCookie('refresh_token');
    const response = {
      message: 'Admin logged out successfully',
      user: updatedAdmin[1][0],
    };
    return response;
  }

  async refreshToken(admin_id: number, refreshToken: string, res: Response) {
    const decodedToken = this.jwtService.decode(refreshToken);
    if (admin_id !== decodedToken['id']) {
      throw new BadRequestException('Admin not found');
    }
    const admin = await this.adminRepo.findOne({ where: { id: admin_id } });
    if (!admin || !admin.hashed_refresh_token) {
      throw new BadRequestException('Admin not found');
    }
    const tokenMatch = await bcrypt.compare(
      refreshToken,
      admin.hashed_refresh_token,
    );

    if (!tokenMatch) {
      throw new ForbiddenException('Forbidden');
    }
    const tokens = await this.getTokens(
      admin.id,
      admin.is_active,
      admin.is_creator,
    );
    const hashedRefreshToken = await bcrypt.hash(tokens.refresh_token, 7);
    const updatedAdmin = await this.adminRepo.update(
      {
        hashed_refresh_token: hashedRefreshToken,
      },
      { where: { id: admin.id }, returning: true },
    );
    res.cookie('refresh_token', tokens.refresh_token, {
      maxAge: 15 * 24 * 60 * 60 * 1000,
      httpOnly: true,
    });
    const response = {
      message: 'Admin is refreshed',
      user: updatedAdmin[1][0],
      tokens: tokens,
    };
    return response;
  }

  async updateAdminPassword(
    id: number,
    updateAdminPassDto: UpdateAdminPassDto,
  ) {
    const admin = await this.adminRepo.findOne({ where: { id } });
    if (!admin) {
      throw new BadRequestException('Admin not found');
    }
    const oldPassword = updateAdminPassDto.old_password;
    const isMatchPass = await bcrypt.compare(
      oldPassword,
      admin.hashed_password,
    );
    if (!isMatchPass) {
      throw new BadRequestException('The password is incorrect');
    }
    if (
      updateAdminPassDto.new_password !==
      updateAdminPassDto.confirm_new_password
    ) {
      throw new BadRequestException('Passwords is not match');
    }
    const hashed_password = await bcrypt.hash(
      updateAdminPassDto.new_password,
      7,
    );
    const updatedAdmin = await this.adminRepo.update(
      { hashed_password: hashed_password },
      { where: { id: admin.id }, returning: true },
    );
    console.log(`New password: ${updateAdminPassDto.new_password}`);
    const response = {
      message: 'Password updated successfully',
      admin: updatedAdmin[1][0],
    };
    return response;
  }

  async activate(id: number) {
    const admin = await this.adminRepo.findOne({ where: { id: id } });
    if (!admin) {
      throw new BadRequestException('Admin not found');
    }
    if (admin.is_active) {
      const updatedAdmin = await this.adminRepo.update(
        { is_active: false },
        { where: { id }, returning: true },
      );
      const response = {
        message: 'Admin deactivated',
        admin: updatedAdmin[1][0],
      };
      return response;
    } else if (!admin.is_active) {
      const updatedAdmin = await this.adminRepo.update(
        { is_active: true },
        { where: { id }, returning: true },
      );
      const response = {
        message: 'Admin activated',
        admin: updatedAdmin[1][0],
      };
      return response;
    }
  }
  findAll() {
    return this.adminRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const admin = await this.adminRepo.findOne({ where: { id: id } });
    if (!admin) {
      throw new BadRequestException('Admin not found');
    }
    return admin;
  }

  async update(id: number, updateAdminDto: UpdateAdminDto) {
    const admin = await this.adminRepo.findOne({ where: { id: id } });
    if (!admin) {
      throw new BadRequestException('Admin not found');
    }
    const updatedAdmin = await this.adminRepo.update(
      { ...updateAdminDto },
      { where: { id }, returning: true },
    );
    const response = {
      message: 'Admin updated successfully',
      admin: updatedAdmin[1][0],
    };
    return response;
  }

  async remove(id: number) {
    const admin = await this.adminRepo.findOne({ where: { id: id } });
    if (!admin) {
      throw new BadRequestException('Admin not found');
    }
    await this.adminRepo.destroy({ where: { id } });
    const response = {
      message: 'Admin removed successfully',
      adminID: id,
    };
    return response;
  }
  async getTokens(admin_id: number, is_active: boolean, is_creator: boolean) {
    const jwtPayload = {
      id: admin_id,
      is_active,
      is_creator,
    };
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(jwtPayload, {
        secret: process.env.ACCESS_TOKEN_KEY,
        expiresIn: process.env.ACCESS_TOKEN_TIME,
      }),
      this.jwtService.signAsync(jwtPayload, {
        secret: process.env.REFRESH_TOKEN_KEY,
        expiresIn: process.env.REFRESH_TOKEN_TIME,
      }),
    ]);
    return {
      access_token: accessToken,
      refresh_token: refreshToken,
    };
  }
}
