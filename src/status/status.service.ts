import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateStatusDto } from './dto/create-status.dto';
import { UpdateStatusDto } from './dto/update-status.dto';
import { Status } from './models/status.model';

@Injectable()
export class StatusService {
  constructor(
    @InjectModel(Status) private readonly statusRepo: typeof Status,
  ) {}
  async create(createStatusDto: CreateStatusDto) {
    const status = await this.statusRepo.findOne({
      where: { name: createStatusDto.name },
    });
    if (status) {
      throw new BadRequestException('This status is already');
    }
    const newStatus = await this.statusRepo.create(createStatusDto);
    const response = {
      message: 'Created status successfully',
      status: newStatus.name,
    };
    return response;
  }

  findAll() {
    return this.statusRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const status = await this.statusRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!status) {
      throw new BadRequestException('Status not found');
    }
    return status;
  }

  async update(id: number, updateStatusDto: UpdateStatusDto) {
    const status = await this.statusRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!status) {
      throw new BadRequestException('Status not found');
    }
    const updatedStatus = await this.statusRepo.update(
      { ...updateStatusDto },
      { where: { id } },
    );
    const response = {
      message: 'Updated status',
      status: updatedStatus['name'],
    };
    return response;
  }

  async remove(id: number) {
    const status = await this.statusRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!status) {
      throw new BadRequestException('Status not found');
    }
    await this.statusRepo.destroy({ where: { id } });
    return `This action removes a #${id} status`;
  }
}
