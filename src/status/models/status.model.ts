import { ApiProperty } from '@nestjs/swagger';
import { Column, DataType, Model, Table } from 'sequelize-typescript';
interface StatusAtrr {
  name: string;
}
@Table({ tableName: 'status' })
export class Status extends Model<Status, StatusAtrr> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 'tasdiqlangan', description: 'Holat nomi' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;
}
