import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { CreateStatusDto } from './create-status.dto';

export class UpdateStatusDto {
  @ApiProperty({ example: 'tasdiqlangan', description: 'Holat nomi' })
  name?: string;
}
