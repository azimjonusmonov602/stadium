import { ApiProperty } from '@nestjs/swagger';

export class CreateStatusDto {
  @ApiProperty({ example: 'tasdiqlangan', description: 'Holat nomi' })
  name: string;
}
