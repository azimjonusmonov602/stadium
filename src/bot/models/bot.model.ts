import { ApiProperty } from '@nestjs/swagger';
import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface BotAttr {
  user_id: number;
  username: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  status: boolean;
}
@Table({ tableName: 'bot' })
export class Bot extends Model<Bot, BotAttr> {
  @ApiProperty({
    example: 123456789,
    description: 'Foydalanuvchining telegram ID kaliti',
  })
  @Column({
    type: DataType.BIGINT,
    primaryKey: true,
    unique: true,
  })
  user_id: number;

  @ApiProperty({
    example: 'username',
    description: 'Foydalanuvchining telegram hisobi',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
  })
  username: string;

  @ApiProperty({ example: 'Ahmad', description: 'Foydalanuvchining ismi' })
  @Column({
    type: DataType.STRING,
  })
  first_name: string;

  @ApiProperty({
    example: 'Shaxmatov',
    description: 'Foydalanuvchining familiyasi',
  })
  @Column({
    type: DataType.STRING,
  })
  last_name: string;

  @ApiProperty({
    example: '+99890123457',
    description: 'Foydalanuvchining telefon raqami',
  })
  @Column({
    type: DataType.STRING,
  })
  phone_number: string;

  @ApiProperty({ example: false, description: 'Foydalanuvchining holati' })
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  status: boolean;
}
