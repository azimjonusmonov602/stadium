import { ApiProperty } from '@nestjs/swagger';
import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface CreateRegionAttr {
  name: string;
}
@Table({ tableName: 'region' })
export class Region extends Model<Region, CreateRegionAttr> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas kalit' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 'Toshkent', description: 'Viloyat nomi' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;
}
