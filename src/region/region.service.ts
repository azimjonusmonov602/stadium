import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { District } from 'src/district/models/district.model';
import { CreateRegionDto } from './dto/create-region.dto';
import { UpdateRegionDto } from './dto/update-region.dto';
import { Region } from './models/region.model';

@Injectable()
export class RegionService {
  constructor(
    @InjectModel(Region) private readonly regionRepo: typeof Region,
    @InjectModel(District) private readonly districtRepo: typeof District,
  ) {}

  async districtInRegion(id: number) {
    const districts = await this.districtRepo.findAll({
      where: { region_id: id },
    });
    return districts;
  }

  async create(createRegionDto: CreateRegionDto) {
    const region = await this.regionRepo.findOne({
      where: { name: createRegionDto.name },
    });
    if (region) {
      throw new BadRequestException('This region already exists');
    }
    const newRegion = await this.regionRepo.create(createRegionDto);
    const response = {
      message: 'Created new region',
      region: newRegion,
    };
    return response;
  }

  findAll() {
    return this.regionRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const region = await this.regionRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!region) {
      throw new BadRequestException('Region not found');
    }
    return region;
  }

  async update(id: number, updateRegionDto: UpdateRegionDto) {
    const region = await this.regionRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!region) {
      throw new BadRequestException('Region not found');
    }
    const updatedRegion = await this.regionRepo.update(
      { ...updateRegionDto },
      { where: { id }, returning: true },
    );
    const response = {
      message: 'Updated region',
      region: updatedRegion,
    };
    return response;
  }

  async remove(id: number) {
    const region = await this.regionRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!region) {
      throw new BadRequestException('Region not found');
    }
    await this.regionRepo.destroy({ where: { id } });
    const response = {
      message: 'Removed region',
      region: region,
    };
    return response;
  }
}
