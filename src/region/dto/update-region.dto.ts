import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { CreateRegionDto } from './create-region.dto';

export class UpdateRegionDto {
  @ApiProperty({ example: 'Toshkent', description: 'Viloyat nomi' })
  name: string;
}
