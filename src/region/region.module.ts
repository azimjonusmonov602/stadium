import { Module } from '@nestjs/common';
import { RegionService } from './region.service';
import { RegionController } from './region.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Region } from './models/region.model';
import { JwtModule } from '@nestjs/jwt';
import { District } from 'src/district/models/district.model';
import { Stadium } from 'src/stadiums/models/stadium.model';

@Module({
  imports: [SequelizeModule.forFeature([Region, District, Stadium]), JwtModule],
  controllers: [RegionController],
  providers: [RegionService],
})
export class RegionModule {}
