import { ApiProperty } from '@nestjs/swagger';

export class CreateMediaDto {
  @ApiProperty({ example: 2, description: 'Tashqi kalit' })
  stadium_id: number;

  @ApiProperty({ example: './photos/media.jpg', description: 'Stadion rasmi' })
  photo: string;

  @ApiProperty({ example: 'description', description: 'Rasm haqida qisqacha' })
  description: string;
}
