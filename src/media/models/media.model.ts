import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { Stadium } from 'src/stadiums/models/stadium.model';

@Table({ tableName: 'media' })
export class Media extends Model<Media> {
  @ApiProperty({ example: 1, description: 'Takrorlanmas' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 2, description: 'Tashqi kalit' })
  @ForeignKey(() => Stadium)
  @Column({
    type: DataType.INTEGER,
  })
  stadium_id: number;
  @BelongsTo(() => Stadium)
  stadium: Stadium;

  @ApiProperty({ example: './photos/media.jpg', description: 'Stadion rasmi' })
  @Column({
    type: DataType.STRING,
  })
  photo: string;

  @ApiProperty({ example: 'description', description: 'Rasm haqida qisqacha' })
  @Column({
    type: DataType.STRING,
  })
  description: string;
}
