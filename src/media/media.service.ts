import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateMediaDto } from './dto/create-media.dto';
import { UpdateMediaDto } from './dto/update-media.dto';
import { Media } from './models/media.model';

@Injectable()
export class MediaService {
  constructor(@InjectModel(Media) private readonly mediaRepo: typeof Media) {}
  async create(createMediaDto: CreateMediaDto) {
    const media = await this.mediaRepo.create(createMediaDto);
    const response = {
      message: 'Created media successfully',
      media: media,
    };
    return media;
  }

  findAll() {
    return this.mediaRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const media = await this.mediaRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!media) {
      throw new BadRequestException('Media not found');
    }
    return media;
  }

  async update(id: number, updateMediaDto: UpdateMediaDto) {
    const media = await this.mediaRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!media) {
      throw new BadRequestException('Media not found');
    }
    const updatedMedia = await this.mediaRepo.update(
      { ...updateMediaDto },
      { where: { id } },
    );
    const response = {
      message: 'Updated media successfully',
      media: updatedMedia,
    };
    return response;
  }

  async remove(id: number) {
    const media = await this.mediaRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!media) {
      throw new BadRequestException('Media not found');
    }
    await this.mediaRepo.destroy({ where: { id } });
    return `This action removes a #${id} media`;
  }
}
