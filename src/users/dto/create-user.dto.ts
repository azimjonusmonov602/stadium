import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsPhoneNumber,
  IsString,
  IsStrongPassword,
  MinLength,
} from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    example: 'Fistonchi',
    description: 'Foydalanuvchi ismi',
  })
  @IsString()
  @IsNotEmpty()
  first_name: string;

  @ApiProperty({
    example: 'Falonchiyev',
    description: 'Foydalanuvchi familiyasi',
  })
  @IsString()
  @IsNotEmpty()
  last_name: string;

  @ApiProperty({
    example: 'user1',
    description: 'Foydalanuvchi nomi',
  })
  @IsString()
  @IsNotEmpty()
  username: string;

  @ApiProperty({
    example: 'password',
    description: 'Foydalanuvchi paroli',
  })
  @IsString()
  @IsNotEmpty()
  @IsStrongPassword()
  @MinLength(6)
  password: string;

  @ApiProperty({
    example: 'confirm_password',
    description: 'Foydalanuvchi parolini tekshirish',
  })
  confirm_password: string;

  @ApiProperty({
    example: 'https://t.me/user',
    description: 'Foydalanuvchi telegram manzili',
  })
  @IsString()
  telegram_link: string;

  @ApiProperty({
    example: 'fistonchi@gmail.com',
    description: 'Foydalanuvchi ismi',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    example: '90 123 45 67',
    description: 'Foydalanuvchi telefon raqami',
  })
  @IsPhoneNumber()
  phone: string;

  @ApiProperty({
    example: '01.01.2000',
    description: "Foydalanuvchi tug'ilgan sanasi",
  })
  @IsNotEmpty()
  @IsDateString()
  birthday: Date;
}
