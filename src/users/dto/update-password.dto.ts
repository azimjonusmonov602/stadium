import { IsStrongPassword } from 'class-validator';

export class UpdatePasswordDto {
  oldPassword: string;
  @IsStrongPassword()
  newPassword: string;
  confirmNewPassword: string;
}
