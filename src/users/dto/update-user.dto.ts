import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from 'class-validator';

export class UpdateUserDto {
  @ApiProperty({
    example: 'Fistonchi',
    description: 'Foydalanuvchi ismi',
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  first_name?: string;

  @ApiProperty({
    example: 'Falonchiyev',
    description: 'Foydalanuvchi familiyasi',
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  last_name?: string;

  @ApiProperty({
    example: 'user1',
    description: 'Foydalanuvchi nomi',
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  username?: string;

  @ApiProperty({
    example: 'password',
    description: 'Foydalanuvchi paroli',
  })
  @ApiProperty({
    example: 'https://t.me/user',
    description: 'Foydalanuvchi pochta manzili',
  })
  telegram_link?: string;

  @ApiProperty({
    example: 'fistonchi@gmail.com',
    description: 'Foydalanuvchi ismi',
  })
  @IsOptional()
  @IsEmail()
  email?: string;

  @ApiProperty({
    example: '90 123 45 67',
    description: 'Foydalanuvchi telefon raqami',
  })
  @IsOptional()
  @IsPhoneNumber()
  phone?: string;

  @ApiProperty({
    example: '01.01.2000',
    description: "Foydalanuvchi tug'ilgan sanasi",
  })
  @IsOptional()
  @IsNotEmpty()
  @IsDateString()
  birthday?: Date;
}
