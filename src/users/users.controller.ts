import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpCode,
  HttpStatus,
  UseGuards,
  Req,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response, Request } from 'express';
import { User } from './models/user.model';
import { LoginUserDto } from './dto/login-user.dto';
import { CookieGetter } from 'src/decorators/cookieGetter.decorator';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { UserGuard } from 'src/guards/user.guard';
import { AdminGuard } from 'src/guards/admin.guard';
import { PhoneUserDto } from './dto/phone-user.dto';
import { VerifyOtpDto } from './dto/verifyOtp.dto';
import { type } from 'os';
import { Otp } from 'src/otp/models/otp.model';

@ApiTags('Users-Foydalanuvchilar')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  @ApiOperation({
    summary:
      "Registration a new user - Yangi foydalanuvchini ro'yxatdan o'tkazish",
  })
  @ApiResponse({ status: 201, type: User })
  @Post('signup')
  registration(
    @Body() createUserDto: CreateUserDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.usersService.registration(createUserDto, res);
  }

  @ApiOperation({ summary: 'Login user - kirish' })
  @ApiResponse({ status: 200, type: User })
  @HttpCode(HttpStatus.OK)
  @Post('signin')
  login(
    @Body() loginUserDto: LoginUserDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.usersService.login(loginUserDto, res);
  }

  @ApiOperation({ summary: 'Logout user - chiqish' })
  @ApiResponse({ status: 200, type: User })
  @HttpCode(HttpStatus.OK)
  @Post('signout')
  logout(
    @CookieGetter('refresh_token') refreshToken: string,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.usersService.logout(refreshToken, res);
  }

  @ApiOperation({ summary: 'Refreshed user - yangilash' })
  @ApiResponse({ status: 200, type: User })
  @HttpCode(HttpStatus.OK)
  @UseGuards(UserGuard)
  @Post(':id/refresh')
  refresh(
    @Param('id') id: string,
    @CookieGetter('refresh_token') refreshToken: string,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.usersService.refreshToken(+id, refreshToken, res);
  }

  @ApiOperation({ summary: 'Updated user password - parolni almashtirish' })
  @ApiResponse({ status: 200, type: User })
  @HttpCode(HttpStatus.OK)
  @Patch(':id/updatePassword')
  password(
    @Param('id') id: string,
    @Body() updatePasswordDto: UpdatePasswordDto,
  ) {
    return this.usersService.updatePass(+id, updatePasswordDto);
  }

  @ApiOperation({ summary: 'Activate User - foydalanuvchini faollashtirish' })
  @ApiResponse({ status: 200, type: User })
  @HttpCode(HttpStatus.OK)
  @Get('/activate/:link')
  activate(@Param('link') link: string) {
    return this.usersService.activate(link);
  }
  @ApiResponse({ status: 200, type: User })
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  @Get('deactivate/:id')
  deactivate(@Param('id') id: string) {
    return this.usersService.deactivate(+id);
  }

  @ApiOperation({
    summary: 'Owner activate - foydalanuvchini ega sifatida faollashtirish',
  })
  @ApiResponse({ status: 200, type: User })
  @HttpCode(HttpStatus.OK)
  @Post(':id/owner')
  is_owner(@Param('id') id: string) {
    return this.usersService.is_owner(+id);
  }

  @ApiOperation({ summary: 'Find all users - barcha foydalanuvchilar' })
  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @ApiOperation({
    summary: 'Find one user by ID - bitta foydalanuvchi ID orqali',
  })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @ApiOperation({
    summary:
      "Update user by ID - Foydalanuvchi ID'si orqali ma'lumotlarini yangilash",
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @ApiOperation({
    summary:
      "Delete user by ID - Foydalanuvchi ID'si orqali ma'lumotlarini o'chirish",
  })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
  @ApiOperation({
    summary:
      'Send user telegram account OTP - Foydalanuvchi telegram hisobiga bir martalik parol yuborish',
  })
  @ApiResponse({ status: 200, type: Otp })
  @Post('/otp')
  newOtp(@Body() phoneUserDto: PhoneUserDto) {
    return this.usersService.newOtp(phoneUserDto);
  }

  @ApiOperation({
    summary: 'Verify telegram account - telegram hisobni tasdiqlash',
  })
  @ApiResponse({ status: 200, type: Otp })
  @Post('/verfy')
  verifyOtp(
    @Body() verifyOtpDto: VerifyOtpDto,
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.usersService.verifyOtp(verifyOtpDto, res);
  }
}
