import { ApiProperty } from '@nestjs/swagger';
import { Table, Model, Column, DataType } from 'sequelize-typescript';

interface UserCreateAttrs {
  first_name?: string;
  last_name?: string;
  username?: string;
  hashed_password?: string;
  telegram_link?: string;
  email?: string;
  phone?: string;
  birthday?: Date;
  is_owner?: boolean;
  is_active?: boolean;
  hashed_refresh_token?: string;
}

@Table({ tableName: 'users' })
export class User extends Model<User, UserCreateAttrs> {
  @ApiProperty({
    example: 1,
    description: 'Unique ID',
  })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    unique: true,
  })
  id: number;

  @ApiProperty({
    example: 'Fistonchi',
    description: 'Foydalanuvchi ismi',
  })
  @Column({
    type: DataType.STRING,
  })
  first_name: string;

  @ApiProperty({
    example: 'Falonchiyev',
    description: 'Foydalanuvchi familiyasi',
  })
  @Column({
    type: DataType.STRING,
  })
  last_name: string;

  @ApiProperty({
    example: 'user1',
    description: 'Foydalanuvchi nomi',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  username: string;

  @ApiProperty({
    example: 'password',
    description: 'Foydalanuvchi paroli',
  })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  hashed_password: string;

  @ApiProperty({
    example: 'https://t.me/user',
    description: 'Foydalanuvchi telegram manzili',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  telegram_link: string;

  @ApiProperty({
    example: 'user1@gmail.com',
    description: 'Foydalanuvchi pochta manzili',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  email: string;

  @ApiProperty({
    example: '90 123 45 67',
    description: 'Foydalanuvchi telefon raqami',
  })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  phone: string;

  @ApiProperty({
    example: '01.01.2000',
    description: "Foydalanuvchi tug'ilgan sanasi",
  })
  @Column({
    type: DataType.DATE,
  })
  birthday: Date;

  @ApiProperty({
    example: 'false',
    description: "Maydon egasi yoki yo'qligi",
  })
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  is_owner: boolean;

  @ApiProperty({
    example: 'false',
    description: 'Foydalanauvchi faol yoki faolmasligi',
  })
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  is_active: boolean;

  @ApiProperty({
    example: 'token',
    description: 'Hashlangan token',
  })
  @Column({
    type: DataType.STRING,
  })
  hashed_refresh_token: string;
  @Column({
    type: DataType.STRING,
  })
  activation_link: string;
}
