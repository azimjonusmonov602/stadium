import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { CreateCommentDto } from './create-comment.dto';

export class UpdateCommentDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  user_id?: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  stadium_id?: number;

  @ApiProperty({ example: 'impression', description: 'Impression' })
  impression?: string;
}
