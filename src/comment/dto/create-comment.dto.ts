import { ApiProperty } from '@nestjs/swagger';

export class CreateCommentDto {
  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  user_id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  stadium_id: number;

  @ApiProperty({ example: 'impression', description: 'Impression' })
  impression: string;
}
