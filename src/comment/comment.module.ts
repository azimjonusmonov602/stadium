import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Comment } from './models/comment.model';
import { User } from 'src/users/models/user.model';
import { Stadium } from 'src/stadiums/models/stadium.model';

@Module({
  imports: [SequelizeModule.forFeature([Comment, User, Stadium])],
  controllers: [CommentController],
  providers: [CommentService],
})
export class CommentModule {}
