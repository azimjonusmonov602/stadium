import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { Comment } from './models/comment.model';

@Injectable()
export class CommentService {
  constructor(
    @InjectModel(Comment) private readonly commentRepo: typeof Comment,
  ) {}
  async create(createCommentDto: CreateCommentDto) {
    const comment = await this.commentRepo.create(createCommentDto);
    const response = {
      message: 'Created comment',
      comment: comment,
    };
    return response;
  }

  findAll() {
    return this.commentRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const comment = await this.commentRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!comment) {
      throw new BadRequestException('Comment not found');
    }
    return comment;
  }

  async update(id: number, updateCommentDto: UpdateCommentDto) {
    const comment = await this.commentRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!comment) {
      throw new BadRequestException('Comment not found');
    }
    const updateComment = await this.commentRepo.update(
      { ...updateCommentDto },
      { where: { id }, returning: true },
    );
    const response = {
      message: 'Update comment',
      comment: updateComment,
    };
    return response;
  }

  async remove(id: number) {
    const comment = await this.commentRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!comment) {
      throw new BadRequestException('Comment not found');
    }
    await this.commentRepo.destroy({ where: { id } });
    return `This action removes a #${id} comment`;
  }
}
