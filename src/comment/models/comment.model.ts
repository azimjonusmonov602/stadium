import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { Stadium } from 'src/stadiums/models/stadium.model';
import { User } from 'src/users/models/user.model';

@Table({ tableName: 'comments' })
export class Comment extends Model<Comment> {
  @ApiProperty({
    example: 1,
    description: 'Takrorlanmas kalit',
  })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
  })
  user_id: number;
  @BelongsTo(() => User)
  user: User;

  @ApiProperty({ example: 1, description: 'Tashqi kalit' })
  @ForeignKey(() => Stadium)
  @Column({
    type: DataType.INTEGER,
  })
  stadium_id: number;
  @BelongsTo(() => Stadium)
  stadium: Stadium;

  @ApiProperty({ example: 'impression', description: 'Impression' })
  @Column({
    type: DataType.STRING,
  })
  impression: string;
}
